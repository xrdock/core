#!/usr/bin/env bash

# 设置各变量
WSPATH=${WSPATH:-'wspath'}
UUID=${UUID:-'58ccefe3-9906-459f-8f69-583d81944a46'}
PORT=${PORT:-'10000'}
WEB_USERNAME=${WEB_USERNAME:-'admin'}
WEB_PASSWORD=${WEB_PASSWORD:-'password'}
WARP_PROXY_DOMAIN=${WARP_PROXY_DOMAIN:-'"geosite:google","geosite:vercel","domain:vercel.ai","geosite:openai","geosite:netflix"'}
RAY_VER=${RAY_VER:-'v25.2.21'}
CFD_VER=${CFD_VER:-'2025.2.0'}
#WARP_ENDPOINT=${WARP_ENDPOINT:-'162.159.192.201:7559'}
# WARP_ALL=${WARP_ALL:-'N'}

DATE_START=$(TZ="Asia/Shanghai" date "+%Y-%m-%d %H:%M:%S")

generate_config() {
#   wget -q https://github.com/peanut996/CloudflareWarpSpeedTest/releases/download/v1.4.3/CloudflareWarpSpeedTest-v1.4.3-linux-amd64.tar.gz
#   tar -zxvf CloudflareWarpSpeedTest-v1.4.3-linux-amd64.tar.gz
#   chmod +x CloudflareWarpSpeedTest
#   ./CloudflareWarpSpeedTest -t 3 -c 500  >/dev/null 2>&1
#   WARP_ENDPOINT=$(awk -F, 'NR==2 {print $1}' "result.csv")
  WARP_ENDPOINT='162.159.192.1:2408'
#   rm -rf CloudflareWarpSpeedTest-v1.4.3-linux-amd64.tar.gz
#   rm -rf CloudflareWarpSpeedTest
#   rm -rf result.csv
  wget -q https://github.com/Loyalsoldier/v2ray-rules-dat/releases/latest/download/geoip.dat
  wget -q https://github.com/Loyalsoldier/v2ray-rules-dat/releases/latest/download/geosite.dat
  
#   WARP_ALL_N=$(cat << EOF
#         {
#             "type":"field",
#             "domain":["geosite:youtube"],
#             "outboundTag":"freedom"
#         },
#         {
#             "type":"field",
#             "domain":[${WARP_PROXY_DOMAIN}],
#             "outboundTag":"WARP"
#         }
# EOF
# )

#   WARP_ALL_Y=$(cat << EOF
#         {
#             "type":"field",
#             "network":"tcp,udp",
#             "outboundTag":"WARP"
#         }
# EOF
# )

#   if [ "$WARP_ALL" = "Y" ]; then
#     ROUTE_RULES=$WARP_ALL_Y
#   else
#     ROUTE_RULES=$WARP_ALL_N
#   fi
  cat > config.json << EOF
{
    "log":{
        "access":"/dev/null",
        "error":"/dev/null",
        "loglevel":"none"
    },
    "inbounds":[
        {
            "port":3001,
            "listen":"127.0.0.1",
			"tag": "vm",
            "protocol":"vmess",
            "settings":{
                "clients":[
                    {
                        "id":"${UUID}",
                        "level":0
                    }
                ],
                "decryption":"none"
            },
            "streamSettings":{
                "network":"ws",
                "security":"none",
                "wsSettings":{
                    "path":"/${WSPATH}-vmess"
                }
            },
            "sniffing":{
                "enabled":true,
                "destOverride":[
                    "http",
                    "tls",
                    "quic"
                ],
                "metadataOnly":false
            }
        },
        {
            "port":3002,
            "listen":"127.0.0.1",
            "protocol":"vmess",
			"tag": "vm-warp",
            "settings":{
                "clients":[
                    {
                        "id":"${UUID}",
                        "alterId":0
                    }
                ]
            },
            "streamSettings":{
                "network":"ws",
                "wsSettings":{
                    "path":"/${WSPATH}-vmess-warp"
                }
            },
            "sniffing":{
                "enabled":true,
                "destOverride":[
                    "http",
                    "tls",
                    "quic"
                ],
                "metadataOnly":false
            }
        },
        {
            "port":3003,
            "listen":"127.0.0.1",
            "tag": "vm-xh",
            "protocol":"vmess",
            "settings":{
                "clients":[
                    {
                        "id":"${UUID}",
                        "alterId":0
                    }
                ]
            },
            "streamSettings":{
                "network":"xhttp",
                "xhttpSettings":{
                    "path":"/${WSPATH}-vmess-xh"
                }
            },
            "sniffing":{
                "enabled":true,
                "destOverride":[
                    "http",
                    "tls",
                    "quic"
                ],
                "metadataOnly":false
            }
        },
        {
            "port":3004,
            "listen":"127.0.0.1",
            "tag": "vm-xh-warp",
            "protocol":"vmess",
            "settings":{
                "clients":[
                    {
                        "id":"${UUID}",
                        "alterId":0
                    }
                ]
            },
            "streamSettings":{
                "network":"xhttp",
                "xhttpSettings":{
                    "path":"/${WSPATH}-vmess-xh-warp"
                }
            },
            "sniffing":{
                "enabled":true,
                "destOverride":[
                    "http",
                    "tls",
                    "quic"
                ],
                "metadataOnly":false
            }
        }
    ],
    "dns":{
        "servers":[
            "https+local://8.8.8.8/dns-query"          
        ]
    },
    "outbounds":[
        {
            "tag":"freedom",
            "protocol":"freedom",
            "settings": {
                "domainStrategy": "ForceIPv4"
            }
        },
        {
            "tag":"WARP",
            "protocol":"wireguard",
            "settings":{
                "secretKey":"kAtQxFS6TZDNgLqteID2F9/hWrvWpVuFABpsaY0ApHI=",
                "address":[
                    "172.16.0.2/32",
                    "2606:4700:110:8505:a958:2021:ab2b:4f0f/128"
                ],
                "peers":[
                    {
                        "publicKey":"bmXOC+F1FxEMF9dyiK2H5/1SUtzH0JuVo51h2wPfgyo=",
                        "allowedIPs":[
                            "0.0.0.0/0",
                            "::/0"
                        ],
                        "endpoint":"${WARP_ENDPOINT}"
                    }
                ],
                "reserved":[56,97,166],
                "kernelMode": false,
                "mtu":1280,
                "domainStrategy": "ForceIPv6v4"
            }
        }
    ],
    "routing":{
        "domainStrategy":"AsIs",
        "rules":[
            {
            "type":"field",
            "inboundTag": ["vm-warp","vm-xh-warp"],
            "outboundTag":"WARP"
            },
            {
            "type":"field",
            "domain":["geosite:youtube"],
            "outboundTag":"freedom"
        },
        {
            "type":"field",
            "domain":[${WARP_PROXY_DOMAIN}],
            "outboundTag":"WARP"
        }
		,{
                "type":"field",
                 "network": "tcp,udp",
                "outboundTag":"freedom"
            }
        ]
    }
}
EOF
}

generate_pm2_file() {

  wget -q https://github.com/XTLS/Xray-core/releases/download/${RAY_VER}/Xray-linux-64.zip -O web.zip
  RELEASE_RANDOMNESS=$(tr -dc 'A-Za-z0-9' </dev/urandom | head -c 6)
  unzip -qq web.zip xray
  upx -1 -q xray >/dev/null 2>&1 
  mv xray ${RELEASE_RANDOMNESS}
  chmod +x /app/${RELEASE_RANDOMNESS}
  rm web.zip

  wget -q https://github.com/cloudflare/cloudflared/releases/download/${CFD_VER}/cloudflared-linux-amd64 -O cfd
  chmod +x cfd
  upx -1 -q cfd >/dev/null 2>&1
  RELEASE_RANDOMNESS_CFD=$(tr -dc 'A-Za-z0-9' </dev/urandom | head -c 6)
  mv cfd ${RELEASE_RANDOMNESS_CFD}
  cat > cdconfig.yaml << EOF
edge-ip-version: auto
no-autoupdate: true
protocol: http2
token: ${ARGO_TOKEN}
loglevel: fatal
EOF 

#   if [[ -n "${ARGO_TOKEN}" ]]; then
#     ARGO_ARGS="tunnel --edge-ip-version auto --no-autoupdate --protocol http2 --loglevel fatal run --token ${ARGO_TOKEN}"
#   else
#     ARGO_ARGS="tunnel --edge-ip-version auto --no-autoupdate --protocol http2 --logfile argo.log --loglevel info --url http://localhost:${PORT}"
#   fi

  cat > ecosystem.config.js << EOF
module.exports = {
  "apps":[
      {
          "name":"web",
          "script":"/app/${RELEASE_RANDOMNESS} run >/dev/null 2>&1"
      },
      {
          "name":"ngx",
          "script":"nginx -c /app/nginx.config -p /app -g 'daemon off;'"
      },
      {
          "name":"ttyd",
          "script":"/app/ttyd",
          "args":"-W -c ${WEB_USERNAME}:${WEB_PASSWORD} -p 2222 bash"
       },
       {
          "name":"filebrowser",
          "script":"/app/filebrowser",
          "args":"--port 3333 --username ${WEB_USERNAME} --password 'PASSWORD_HASH' --baseurl /${WSPATH}-fb"
        },
        {
          name: 'argo',
          script: '${RELEASE_RANDOMNESS_CFD}',
          args: "tunnel -config ./cdconfig.yaml run"
        }
    ]
}
EOF
}



generate_nginx_file() {

cat > nginx.config << EOF

worker_processes  auto;
worker_rlimit_nofile 10240;
error_log         /dev/null;

events {
    worker_connections  10240;
}

http {
    
    default_type                   application/octet-stream;
    sendfile                       on;
    keepalive_timeout              3000s;
    server_tokens                  off;
    resolver                       8.8.8.8 valid=30s;

    client_max_body_size           20480m;

    server {
        listen               ${PORT} default_server;
        listen               [::]:${PORT} default_server;
        server_name          _;
        charset              utf-8;
        root                 html;
        access_log        off;
    
		
	    error_page 404 /50x.html;
	    error_page 500 502 503 504 /50x.html;
  
	    location = /50x.html{
		    default_type text/plain;
		    return 200 'error';
	    }

        location = / {
            default_type 'text/html; charset=utf-8';
            return 200 '2001-${DATE_START}'; 
        }

        location = /health {
            default_type 'text/html; charset=utf-8';
            return 200 '2001-${DATE_START}'; 
        }


        location /${WSPATH}-vmess {
            proxy_redirect off;
            proxy_pass http://127.0.0.1:3001;
            proxy_http_version 1.1;
            proxy_set_header Upgrade \$http_upgrade;
            proxy_set_header Connection upgrade;
            proxy_set_header Host \$http_host;
            proxy_read_timeout 1d;
        }

        location /${WSPATH}-vmess-warp {
            proxy_redirect off;
            proxy_pass http://127.0.0.1:3002;
            proxy_http_version 1.1;
            proxy_set_header Upgrade \$http_upgrade;
            proxy_set_header Connection upgrade;
            proxy_set_header Host \$http_host;
            proxy_read_timeout 1d;
        }
		
		location /${WSPATH}-vmess-xh {
            client_max_body_size 0;
            grpc_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            client_body_timeout 5m;
            grpc_read_timeout 315;
            grpc_send_timeout 5m;
            grpc_pass grpc://127.0.0.1:3003;
        }
		
		location /${WSPATH}-vmess-xh-warp {
            client_max_body_size 0;
            grpc_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            client_body_timeout 5m;
            grpc_read_timeout 315;
            grpc_send_timeout 5m;
            grpc_pass grpc://127.0.0.1:3004;
        }

        location /${WSPATH}-ttyd {
            proxy_redirect off;
            proxy_pass http://127.0.0.1:2222;
            proxy_http_version 1.1;
            proxy_set_header Host \$host;
            proxy_set_header X-Forwarded-Proto \$scheme;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_set_header Upgrade \$http_upgrade;
            proxy_set_header Connection "upgrade";
            rewrite ^/${WSPATH}-ttyd(.*) /\$1 break;
            proxy_read_timeout 1d;
        }

        location /${WSPATH}-fb {
            proxy_redirect off;
            proxy_pass http://127.0.0.1:3333;
            proxy_http_version 1.1;
            proxy_set_header Upgrade \$http_upgrade;
            proxy_set_header Connection upgrade;
            proxy_set_header Host \$http_host;
            proxy_read_timeout 1d;
        }

    }
}

EOF
}

generate_ttyd() {
  cat > ttyd.sh << EOF
#!/usr/bin/env bash

check_run() {
  [[ \$(pgrep -lafx ttyd) ]] && echo "ttyd 正在运行中" && exit
}

download_ttyd() {
  if [ ! -e ttyd ]; then
    URL=\$(wget -qO- "https://api.github.com/repos/tsl0922/ttyd/releases/latest" | grep -o "https.*x86_64")
    URL=\${URL:-https://github.com/tsl0922/ttyd/releases/download/1.7.7/ttyd.x86_64}
    wget -q -O ttyd \${URL}
    chmod +x ttyd
  fi
}
check_run
download_ttyd
EOF
gzexe ttyd.sh >/dev/null 2>&1
rm -r ttyd.sh~
}

generate_filebrowser () {
  cat > filebrowser.sh << EOF
#!/usr/bin/env bash

check_run() {
  [[ \$(pgrep -lafx filebrowser) ]] && echo "filebrowser 正在运行中" && exit
}


download_filebrowser() {
  if [ ! -e filebrowser ]; then
    URL=\$(wget -qO- "https://api.github.com/repos/filebrowser/filebrowser/releases/latest" | grep -o "https.*linux-amd64.*gz")
    URL=\${URL:-https://github.com/filebrowser/filebrowser/releases/download/v2.30.0/linux-amd64-filebrowser.tar.gz}
    wget -q -O filebrowser.tar.gz \${URL}
    tar xzvf filebrowser.tar.gz filebrowser
    rm -f filebrowser.tar.gz
    chmod +x filebrowser
    PASSWORD_HASH=\$(./filebrowser hash ${WEB_PASSWORD})
    sed -i "s#PASSWORD_HASH#\$PASSWORD_HASH#g" ecosystem.config.js
  fi
}

check_run
download_filebrowser
EOF
gzexe filebrowser.sh >/dev/null 2>&1
rm -r filebrowser.sh~
}

keepWeb() {
    while true; 
    do curl -s "${RENDER_EXTERNAL_URL}" && echo && sleep 180; 
    done
}

generate_config
generate_ttyd
generate_nginx_file
generate_pm2_file
generate_filebrowser
[ -e ttyd.sh ] && bash ttyd.sh
[ -e filebrowser.sh ] && bash filebrowser.sh
keepWeb &
[ -e ecosystem.config.js ] && pm2 start
sleep infinity
